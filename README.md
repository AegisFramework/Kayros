# Kayros

Kayros is a simple CSS and components library.

## Default Variables

```css
:root {
	--color-primary: #f75c5f;

	--color-text: #000000;
	--color-subtext: #868686;

	--color-input: #66615b;
	--color-input-disabled: #bfc7d1;
	--color-input-background: #fff;
	--color-input-background-disabled: #f8f9fa;

	--color-border: #ddd;
	--color-border-hover: #b4bdc9;
	--color-border-active: #a9b3c1;

	--breakpoint-small: 30rem;
	--breakpoint-phablet: 37.56255rem;
	--breakpoint-tablet: 62rem;
	--breakpoint-desktop: 75rem;
	--breakpoint-desktop-large: 120rem;
	--breakpoint-retina: 160rem;
	--breakpoint-4k: 240rem;
	--breakpoint-5k: 312.5rem;
	--breakpoint-8k: 500rem;

	--box-shadow-default: 0px 6px 6px hsla(0, 0%, 73%, 0.5);
	--box-shadow-extra: 0 13px 22px rgba(0, 0, 0, 0.01), 0 3px 12px rgba(0, 0, 0, 0.1);
}
```

## Breakpoints

```css
/** Extra Small Devices, Phones (480px) **/
@media screen and (min-width : 30em) {}

/** Medium Screens, Phablets (601px) **/
@media screen and (min-width: 37.56255em) {}

/** Medium Devices, Tablets (992px)**/
@media screen and (min-width: 62em) {}

/** HD Screen, Large Devices, Wide Screens, Desktop (1200px) **/
@media screen and (min-width: 75em) {}

/** Full HD Screen, Large Devices, Wide Screens, Large Desktops (1920px) **/
@media screen and (min-width: 120em) {}

/** Retina Screen , Large Devices, Wide Screens(2560px) **/
@media screen and (min-width: 160em) {}

/** 4k Screens, Large Devices, Wide Screens (3840px) **/
@media screen and (min-width: 240em) {}

/** 5k Screens, Large Devices, Wide Screens (5000px) **/
@media screen and (min-width: 312.5em) {}

/** 8k Screens, Large Devices, Wide Screens (8000px) **/
@media screen and (min-width: 500em) {}
```

## Components

### Tabs

```html
<nav>
	<ul class="tabs">
		<li class="tab tab--active">Home</li>
		<li class="tab">About</li>
	</ul>
</nav>
```


### Card

```html
<div class="card">
	<div class="card__header">
		<h2>My Card<h2>
	</div>
	<div class="card__body">
		<p>My content</p>
	</div>
	<div class="card__footer">
		<p><small>My Details</small></p>
	</div>
</div>
```

## License
Kayros is released under the [MIT License](https://gitlab.com/AegisFramework/Kayros/blob/master/LICENSE)